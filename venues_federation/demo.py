import random

import frappe
from frappe.utils import add_days, now_datetime
from frappe.utils.make_random import get_random

def demo():
	frappe.flags.in_demo = 1
	initalize_site()
	add_venues()
	setup_website()
	create_checkins()

def initalize_site():
	print("Initialization...")
	from frappe.desk.page.setup_wizard.setup_wizard import setup_complete
	if not frappe.get_all('Company', limit=1):
		completed = setup_complete({
			"full_name": "Cécile Provencher",
			"email": "cecile@dokos.io",
			"company_tagline": 'Fédération de Tiers Lieux',
			"password": "tierslieux",
			"fy_start_date": "2021-01-01",
			"fy_end_date": "2021-12-31",
			"bank_account": "Banque Nationale",
			"domains": ["Venue"],
			"company_name": "Fédération de Tiers Lieux",
			"chart_of_accounts": "Plan Comptable Général",
			"company_abbr": "FTL",
			"currency": 'EUR',
			"timezone": 'Europe/Paris',
			"country": 'France',
			"language": "Français",
			"lang": "fr",
			"demo": 1
		})
		print("Initialization completed", completed)

def setup_system_settings():
	frappe.db.set_value("System Settings", None, "time_format", "HH:mm")

def add_venues():
	print("Adding venues...")
	venues = [
		{
			"doctype": "Federated Venue",
			"label": "Mappemonde",
			"logo": "/assets/venues_federation/images/demo/mappemonde.png"
		},
		{
			"doctype": "Federated Venue",
			"label": "Dokompany",
			"logo": "/assets/venues_federation/images/demo/dokompany.png"
		},
		{
			"doctype": "Federated Venue",
			"label": "Magna",
		},
		{
			"doctype": "Federated Venue",
			"label": "Wise",
		},
		{
			"doctype": "Federated Venue",
			"label": "CompuAdd",
		},
		{
			"doctype": "Federated Venue",
			"label": "Gamma Realty",
		}
	]
	for venue in venues:
		doc = frappe.get_doc(venue)
		doc.insert(ignore_if_duplicate=True)

def setup_website():
	doc = frappe.get_single("Website Settings")
	doc.home_page = "login"
	doc.disable_signup = True
	doc.save()

def create_checkins():
	customers = frappe.parse_json(open(frappe.get_app_path('venues_federation', 'data', 'customers.json')).read())
	current_date = add_days(now_datetime(), -50)

	for i in range(50):
		for j in range(1, random.randint(1, 200)):
			customer = customers[random.randint(1, 450)]
			client_venue = get_random("Federated Venue")
			provider_venue = get_random("Federated Venue", filters={"name": ("!=", client_venue)})

			for k in range(0, 2):
				checkin_time = current_date.replace(hour=random.randint(7, 13) if k == 1 else random.randint(14, 19), minute=0)

				frappe.get_doc({
					"doctype": "Federated Venue Checkin",
					"datetime": checkin_time,
					"client_venue": client_venue,
					"provider_venue": provider_venue,
					"customer": customer.get("customer_name"),
					"user": customer.get("customer_name")
				}).insert()

		current_date = add_days(current_date, 1)