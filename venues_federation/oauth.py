import json
import frappe
from frappe import _

from frappe.utils.oauth import get_email, get_first_name, get_last_name

def login_oauth_user(data=None, provider=None, state=None, email_id=None, key=None, generate_login_token=False):
	if isinstance(data, str):
		data = json.loads(data)

	if not (state and state["token"]):
		frappe.respond_as_web_page(_("Invalid Request"), _("Token is missing"), http_status_code=417)
		return

	user = get_email(data)

	if not user:
		frappe.respond_as_web_page(_("Invalid Request"), _("Please ensure that your profile has an email address"))
		return

	if update_oauth_user(user, data, provider) is False:
		return

	frappe.local.login_manager.user = user
	frappe.local.login_manager.post_login()

	# because of a GET request!
	frappe.db.commit()

	login_token = frappe.generate_hash(length=32)
	frappe.cache().set_value("login_token:{0}".format(login_token), frappe.local.session.sid, expires_in_sec=120)

	frappe.response["login_token"] = login_token

	return login_token


def update_oauth_user(user, data, provider):
	if isinstance(data.get("location"), dict):
		data["location"] = data.get("location").get("name")

	save = False

	if not frappe.db.exists("User", user):
		save = True
		user = frappe.new_doc("User")

		gender = data.get("gender", "").title()

		if gender and not frappe.db.exists("Gender", gender):
			doc = frappe.new_doc("Gender", {"gender": gender})
			doc.insert(ignore_permissions=True)

		user.update({
			"doctype": "User",
			"first_name": get_first_name(data),
			"last_name": get_last_name(data),
			"email": get_email(data),
			"gender": gender,
			"enabled": 1,
			"new_password": frappe.generate_hash(get_email(data)),
			"location": data.get("location"),
			"user_type": "Website User",
			"user_image": data.get("picture") or data.get("avatar_url")
		})

	else:
		user = frappe.get_doc("User", user)
		if not user.enabled:
			frappe.respond_as_web_page(_('Not Allowed'), _('User {0} is disabled').format(user.email))
			return False

	if not user.get_social_login_userid(provider):
		save = True
		user_id_property = frappe.db.get_value("Social Login Key", provider, "user_id_property") or "sub"
		user.set_social_login_userid(provider, userid=data[user_id_property])

	if save:
		user.flags.ignore_permissions = True
		user.flags.no_welcome_mail = True

		# set default signup role as per Portal Settings
		default_role = frappe.db.get_single_value("Portal Settings", "default_role")
		if default_role:
			user.add_roles(default_role)

		user.save()