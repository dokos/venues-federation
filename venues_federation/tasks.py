import frappe

from venues_federation.venues_federation.doctype.federated_venue_checkin_summary.federated_venue_checkin_summary import find_and_associate_venue_checkin

def summarize_pending_checkins() -> None:
	for checkin in frappe.get_all("Federated Venue Checkin",
		filters={"venue_checkin_summary": ("is", "not set")},
		pluck="name"
	):
		doc = frappe.get_doc("Federated Venue Checkin", checkin)
		find_and_associate_venue_checkin(doc)