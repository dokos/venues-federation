# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
# MIT License. See license.txt

import base64
import json

import frappe
from frappe import _
from frappe.utils.oauth import get_oauth2_flow, get_redirect_uri, get_oauth2_providers

from venues_federation.venues_federation.doctype.federated_venue.federated_venue import does_auth_provider_redirect_to_checkin

no_cache = True

def get_context(context):
	context.no_header = True
	context.full_width = True

	# On récupère l'éventuel cookie pour le fournisseur par défaut
	# https://www.cnil.fr/fr/cookies-et-autres-traceurs/regles/cookies/que-dit-la-loi
	last_login_provider_id = frappe.local.request.cookies.get("fv_last_selected_venue", None)

	# On récupère le lieu client où l'utilisateur est présent physiquement
	# Les QR Code sont plus compacts lorsqu'ils sont en majuscules,
	# on prend soin de mettre l'identifiant en minuscules
	# car les clés primaires des lieux sont en minuscules
	client_venue_id = (frappe.form_dict.get("client", None) or "").lower()

	context.last_login_provider_id = last_login_provider_id
	context.client_venue_id = client_venue_id

	def sort_key(x):
		return (x.identification != last_login_provider_id, x.identification != client_venue_id, x.label)

	venues = frappe.get_all("Federated Venue",
		filters={"social_login_key": ("is", "set")},
		fields=[
			"name",
			"identification",
			"social_login_key",
			"base_url",
			"label",
			"logo",
			"address",
			"email",
			"phone",
		],
		order_by="label ASC"
	)
	valid_clients = {sl.identification for sl in venues}

	if not client_venue_id or client_venue_id not in valid_clients:
		frappe.redirect_to_message(
			_('Invalid link'),
			_("Ce lien n'est pas valide.<br>Veuillez contacter votre tiers lieu.") + f"\n<br><details>{frappe.request.url}</details>",
		)
		frappe.local.flags.redirect_location = frappe.local.response.location
		raise frappe.Redirect

	providers = sorted(venues, key=sort_key)
	here = None

	for venue in providers:
		c_venue = client_venue_id
		p_venue = venue.identification

		if does_auth_provider_redirect_to_checkin(venue.social_login_key):
			# The OAuth redirect URL already perfoms the checkin and access confirmation.
			# Thus the `redirect_url` includes the Social Login Key's ID.
			redirect_to = None
		else:
			# This is a SSO or builtin login: the redirect URL must not perform the checkin,
			# and the user will be checked against the whitelist
			# url = f"api/method/venues_federation.api.action.{default_action}?host_id={venue}&supplier_id={provider}"
			url = f"api/method/venues_federation.api.interactive_checkin?client_venue={c_venue}&provider_venue={p_venue}"
			redirect_to = frappe.utils.get_url(url)

		venue.auth_url = get_oauth2_authorize_url(venue.social_login_key, redirect_to, client_venue_id)
		venue.is_here = c_venue == venue.identification
		venue.is_last = last_login_provider_id == venue.identification

		if venue.is_here:
			here = venue

	context["here"] = here
	context["social_logins"] = providers

def get_oauth2_authorize_url(provider, redirect_to, venue):
	flow = get_oauth2_flow(provider)

	if not redirect_to:
		pass

	state = { "site": frappe.utils.get_url(), "token": frappe.generate_hash(), "redirect_to": redirect_to, "venue": venue }

	# relative to absolute url
	data = {
		"redirect_uri": get_redirect_uri(provider),
		"state": base64.b64encode(bytes(json.dumps(state).encode("utf-8")))
	}

	oauth2_providers = get_oauth2_providers()

	# additional data if any
	data.update(oauth2_providers[provider].get("auth_url_data", {}))

	return flow.get_authorize_url(**data)
