import base64
import json
from typing import TYPE_CHECKING
from werkzeug.exceptions import NotFound

import frappe
import requests
from frappe import _
from frappe.utils.data import now_datetime

from venues_federation.oauth import login_oauth_user

if TYPE_CHECKING:
	from venues_federation.venues_federation.doctype.federated_venue_checkin.federated_venue_checkin import FederatedVenueCheckin


@frappe.whitelist(allow_guest=True)
def v1():
	API_PATH = "venues_federation.api.v1"
	path = str(frappe.local.request.path).split(API_PATH, 1)[1]
	frappe.local.response.http_status_code = 200

	http_method = frappe.local.request.method

	if http_method == "POST":
		return handle_post_methods(path)

	elif http_method == "GET":
		return handle_get_methods(path)

	raise NotFound


def handle_post_methods(path):
	if path.startswith("/checkin"):

		return checkin(
			client_venue=frappe.form_dict.get("client_venue"),
			provider_venue=frappe.form_dict.get("provider_venue"),
			customer=frappe.form_dict.get("customer"),
			user=frappe.form_dict.get("user")
		)

	raise NotFound


def handle_get_methods(path):
	if path.startswith("/venue"):
		venues = frappe.get_list("Federated Venue", pluck="name")
		# TODO: Performance improvements
		return [frappe.get_cached_doc("Federated Venue", venue).as_dict(no_default_fields=True) for venue in venues]

	raise NotFound


@frappe.whitelist(allow_guest=True)
def login_and_checkin(code, state):
	path = frappe.request.path[1:].split("/")
	if len(path) == 4 and path[3]:
		provider = path[3]

		if isinstance(state, str):
			state = base64.b64decode(state)
			state = json.loads(state.decode("utf-8"))

		social_login_key = provider
		if not frappe.db.exists("Social Login Key", social_login_key):
			social_login_key = frappe.db.get_value("Federated Venue", {"identification": provider}, "social_login_key")

		if social_login_key and frappe.db.exists("Social Login Key", social_login_key):
			try:
				token = get_access_token(social_login_key, code)
				info = get_user_info(social_login_key, token)

				if not verify_access(info, provider, token):
					frappe.response.http_status_code = 403
					frappe.log_error("Venue Unauthorized Access", f"User: {info!r}, Provider: {provider!r}, Login: {social_login_key!r}")
					return frappe.respond_as_web_page(
						_("Error"),
						_(
							"You are not authorized to use this service. Please contact the venue handling your subscription."
						),
						indicator_color="red",
					)

				login_oauth_user(info, provider=social_login_key, state=state)
				checkin(state.get("venue"), provider, user=info.get("email"))
				store_last_selected_venue_cookie(provider)
				frappe.db.commit()

				frappe.respond_as_web_page(
					_("Checkin/checkout successful"),
					_("Your action has been successfully recorded."),
					indicator_color="green",
					primary_action="/checkins",
				)
			except Exception:
				frappe.log_error()
				frappe.response.http_status_code = 500
				frappe.respond_as_web_page(
					_("Error"),
					_("Your action could not be successfully recorded. Please contact us."),
					indicator_color="red",
				)
		else:
			frappe.response.http_status_code = 404
			return "No such provider"
	else:
		frappe.response.http_status_code = 400
		return "Invalid path"


def get_access_token(provider: str, code: str) -> dict:
	"""
	Get access token from Oauth provider
	"""
	from frappe.integrations.oauth2 import encode_params
	from frappe.utils.oauth import get_oauth2_providers, get_oauth_keys

	keys = get_oauth_keys(provider)

	providers = get_oauth2_providers()

	access_token_url = (
		providers.get(provider, {}).get("flow_params", {}).get("access_token_url")
	)
	token_response = requests.post(
		access_token_url,
		headers={"content-type": "application/x-www-form-urlencoded"},
		data=encode_params(
			{
				"grant_type": "authorization_code",
				"code": code,
				"redirect_uri": frappe.utils.get_url(
					providers.get(provider, {}).get("redirect_uri")
				),
				"client_id": keys.get("client_id"),
				"client_secret": keys.get("client_secret"),
				"scope": "all",
			}
		),
	)

	response = token_response.json()
	if response.get("error"):
		frappe.log_error(
			_("Federated Venue Connection"), f"{provider}: {response.get('error')} - {response.get('error_description')}"
		)

	return response


def verify_access(info: dict, provider: str, token: dict) -> bool:
	"""
	Verifies if access can be given
	"""

	if verify_access_from_whitelist(info, provider):
		return True

	base_url, access_confirmation_api = frappe.db.get_value(
		"Federated Venue", provider, ["base_url", "access_confirmation_api"]
	)

	if access_confirmation_api:
		try:
			access = requests.post(
				f"{base_url}{access_confirmation_api}",
				headers={"Authorization": f"Bearer {token.get('access_token')}"},
			)
			if access.status_code != 200:
				frappe.log_error(
					"access_confirmation_api",
					f"{provider}: {access.status_code}\n{access.text}",
				)
			elif access.json().get("message"):
				return True
		except Exception:
			frappe.log_error()

	return False


def verify_access_from_whitelist(info, provider: str):
	return bool(
		frappe.db.exists(
			"Federated Venue Whitelist", dict(venue=provider, email=info.get("email"))
		)
	)


def get_user_info(provider: str, token: str) -> dict:
	base_url, api_endpoint = frappe.db.get_value(
		"Social Login Key", provider, ["base_url", "api_endpoint"]
	)
	if not api_endpoint:
		return False
	user_info = requests.post(
		f"{base_url}{api_endpoint}",
		headers={"Authorization": f"Bearer {token.get('access_token')}"},
	)

	return user_info.json()


@frappe.whitelist(allow_guest=True)
def get_checkin_url(identification: str):
	return frappe.db.get_value("Venue", identification, "checkin_url")


@frappe.whitelist(methods=["POST"])
def checkin(
	client_venue: str,
	provider_venue: str,
	user: str,
	customer: str | None = None,
):
	if not check_user(user):
		return

	checkin_doc = checkin_or_checkout(
		client_venue=client_venue,
		provider_venue=provider_venue,
		user=user,
		customer=customer,
		ignore_permissions=True,
	)

	return checkin_doc.as_dict(no_default_fields=True)


@frappe.whitelist(methods=["GET"], allow_guest=True)
def interactive_checkin(
	client_venue: str,
	provider_venue: str,
):
	user: str = frappe.session.user  # type: ignore
	if not check_user(user) or not verify_sso_user(user, provider_venue):
		return

	checkin_doc = checkin_or_checkout(
		client_venue=client_venue,
		provider_venue=provider_venue,
		user=user,
		ignore_permissions=True,
	)

	frappe.db.commit()
	# frappe.local.response["location"] = f"/fv/success?status={checkin_doc.status}"  # TODO: data model refactor
	frappe.local.response["location"] = "/fv/success"
	frappe.local.response["type"] = "redirect"
	store_last_selected_venue_cookie(provider_venue)


def store_last_selected_venue_cookie(provider: str):
	if hasattr(frappe.local, "cookie_manager"):
		frappe.local.cookie_manager.set_cookie("fv_last_selected_venue", provider)


def check_user(pkey: str | None):
	PKEY = "email"
	has_access_to_user = pkey and bool(frappe.get_list("User", filters={PKEY: pkey}, limit=1))

	if not has_access_to_user:
		frappe.local.response = frappe._dict({
			"error": f"Unauthorized access to user {(pkey or '<empty>')[:128]!r}",
			"http_status_code": 403,
		})
		return False
	return True


def verify_sso_user(pkey: str, provider: str):
	is_allowed = False
	if frappe.db.exists("Federated Venue Whitelist", dict(venue=provider, email=pkey)):
		# Verify access to venue based on whitelist
		is_allowed = True

	if not is_allowed:
		frappe.response.http_status_code = 402
		frappe.respond_as_web_page(
			_("Error"),
			_(
				"You are not authorized to use this service. Please contact the venue handling your subscription."
			),
			indicator_color="red",
		)
		return False
	return True


def checkin_or_checkout(
	client_venue: str,
	provider_venue: str,
	user: str,
	customer: str | None = None,
	ignore_permissions=False,
):
	checkin_doc: "FederatedVenueCheckin" = frappe.get_doc(
		{
			"doctype": "Federated Venue Checkin",
			"datetime": now_datetime(),
			"client_venue": client_venue,
			"provider_venue": provider_venue,
			"customer": customer or user,
			"user": user,
		}
	).insert(ignore_permissions=ignore_permissions)  # type: ignore
	return checkin_doc

# TODO:
# Renvoi booléen sur l'API checkin
# Associer automatiquement le provider venue
# Association d'un identifiant temporaire avec un utilisateur + renvoi au broker


""""
FLOW
1. User creation
2. Customer creation/association with an user => callback to CITC
3. Registration => Checkin API
4. User deletion
5. Association with another customer

CITC DATA
|place_id|user_id|customer_id|card_id|

-> Data are created locally => Sent to CITC => Associated with ID
-> Data can be deleted locally => Deleted at CITC
"""


"""
Oauth Redirect URI: {BASE URL}/api/method/pass_tiers_lieux.api.login_and_checkin/{place_id}
"""
