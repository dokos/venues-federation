frappe.ready(function() {
	frappe.web_form.after_load = () => {
		frappe.call({
			method: "frappe.client.get_list",
			args: {
				doctype: "Federated Venue",
				order_by: "label",
				fields: ["name as value", "label"],
			}
		}).then(r => {
			const venues = Object.assign({}, ...r.message.map((res) => ({[res.value]: res.label})));
			frappe.web_form.on("venue", (field, value) => {
				frappe.web_form.set_value("venue_name", venues[value])
			})

			frappe.web_form.set_df_property("venue", "options", r.message);
			frappe.web_form.get_field("venue").set_options()
			frappe.web_form.set_value("venue", r.message[0].value)

			if (r.message.length == 1) {
				frappe.web_form.set_df_property('venue', 'read_only', 1);
			}
		})
	}
})


