# Copyright (c) 2022, Dokos SAS and contributors
# For license information, please see license.txt

import frappe
from frappe.website.website_generator import WebsiteGenerator


class FederatedVenueWhitelist(WebsiteGenerator):
	# begin: auto-generated types
	# This code is auto-generated. Do not modify anything in this block.

	from typing import TYPE_CHECKING

	if TYPE_CHECKING:
		from frappe.types import DF

		email: DF.Data
		invite_as_user: DF.Check
		venue: DF.Link
		venue_name: DF.Data | None
	# end: auto-generated types

	def has_webform_permission(self):
		return frappe.db.exists(
			"Federated Venue Managers",
			dict(parenttype="Federated Venue", parent=self.venue, user=frappe.session.user),
		)

	def after_insert(self):
		self._invite()

	def _invite(self):
		if not self.invite_as_user:
			return
		if frappe.db.exists("User", self.email):
			return

		user = frappe.get_doc(
			{
				"doctype": "User",
				"first_name": self.email,
				"email": self.email,
				"user_type": "Website User",
				"send_welcome_email": 1,
			}
		)
		user.insert(ignore_permissions=True)
