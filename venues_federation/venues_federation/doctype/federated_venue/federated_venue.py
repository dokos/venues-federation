# Copyright (c) 2021, Dokos SAS and contributors
# For license information, please see license.txt

import frappe
import frappe.permissions
from frappe.website.website_generator import WebsiteGenerator


def get_qrcode_image(url: str, type = "svg", *, scale=4, fg="#000", bg="#fff"):
	from io import BytesIO, StringIO
	from pyqrcode import create as qrcreate

	qr = qrcreate(url)
	stream = BytesIO()
	try:
		if type == "svg":
			qr.svg(stream, scale=scale, background=bg, module_color=fg)
			return stream.getvalue().decode().replace("\n", "")
		elif type == "png":
			qr.png(stream, scale=scale, background=bg, module_color=fg)
			return stream.getvalue()
		elif type == "eps":
			stream = StringIO()
			qr.eps(stream, scale=scale, background=bg, module_color=fg)
			return stream.getvalue()
	finally:
		stream.close()
	return None


class FederatedVenue(WebsiteGenerator):
	# begin: auto-generated types
	# This code is auto-generated. Do not modify anything in this block.

	from typing import TYPE_CHECKING

	if TYPE_CHECKING:
		from frappe.types import DF
		from venues_federation.venues_federation.doctype.federated_venue_managers.federated_venue_managers import FederatedVenueManagers
		from venues_federation.venues_federation.doctype.fv_about_card_row.fv_about_card_row import FVAboutCardRow
		from venues_federation.venues_federation.doctype.fv_about_equipment_row.fv_about_equipment_row import FVAboutEquipmentRow

		about_access: DF.SmallText | None
		about_accessibility: DF.SmallText | None
		about_activities: DF.SmallText | None
		about_card_1_list: DF.Table[FVAboutCardRow]
		about_card_1_subtitle: DF.Data | None
		about_card_1_title: DF.Data | None
		about_card_2_list: DF.Table[FVAboutCardRow]
		about_card_2_subtitle: DF.Data | None
		about_card_2_title: DF.Data | None
		about_card_3_list: DF.Table[FVAboutCardRow]
		about_card_3_subtitle: DF.Data | None
		about_card_3_title: DF.Data | None
		about_description: DF.TextEditor | None
		about_equipments: DF.Table[FVAboutEquipmentRow]
		about_image: DF.AttachImage | None
		about_schedule: DF.SmallText | None
		access_confirmation_api: DF.Data | None
		address: DF.SmallText | None
		api_user: DF.Link | None
		base_url: DF.Data | None
		client_id: DF.Data | None
		client_secret: DF.Password | None
		connection_url: DF.SmallText | None
		disabled: DF.Check
		email: DF.Data | None
		identification: DF.Data | None
		label: DF.Data
		logo: DF.AttachImage | None
		managers: DF.Table[FederatedVenueManagers]
		phone: DF.Data | None
		published: DF.Check
		route: DF.Data | None
		social_login_key: DF.Link | None
		webhook: DF.Link | None
	# end: auto-generated types

	def onload(self):
		if self.social_login_key:
			if not does_auth_provider_redirect_to_checkin(self.social_login_key):
				self.set_onload("has_sso_login", True)
		super().onload()

	def get_context(self, context):
		# context.no_header = True
		context.full_width = True
		context.no_cache = True
		context.venue = self
		context.title = self.label

	def autoname(self):
		if self.identification:
			self.identification = frappe.scrub(self.identification)
		else:
			self.identification = frappe.generate_hash(self.label, 15)

		self.identification = self.identification.lower()  # Ensure lower-case

	def validate(self):
		if not self.disabled:
			self.generate_social_login_key()
			self.connection_url = f"{frappe.utils.get_url()}/federated_login?client={self.name}"
		else:
			self.delete_social_login_key()
			self.connection_url = None

		self.published = not self.disabled
		super().validate()

	def on_update(self):
		# Add role `Federated Venue Manager` to the users in the `managers` table
		# if (self.get_latest() or {}).get("managers"):
		# 	for name in frappe.get_all(
		# 		"User Permission",
		# 		filters={
		# 			"allow": "Federated Venue",
		# 			"for_value": self.name,
		# 			"user": ("not in", [m.user for m in self.managers])
		# 		},
		# 		pluck="name",
		# 	):
		# 		frappe.delete_doc("User Permission", name, ignore_permissions=True)

		for m in self.managers:
			if m.user:
				u = frappe.get_doc("User", m.user)
				u.update({"role_profiles": [], "role_profile_name": ""})
				u.append_roles("Federated Venue Manager")
				u.save(ignore_permissions=True)
				if not any(p.role == "System Manager" for p in u.roles):
					# Create a user permission to restrict the user to only see the venue
					frappe.permissions.add_user_permission("Federated Venue", self.name, m.user, ignore_permissions=True)
		return super().on_update()

	def generate_social_login_key(self):
		if not (self.client_id and self.client_secret and self.base_url):
			return

		self.create_update_social_login()

	def create_update_social_login(self):
		frappe.only_for(("System Manager", "Federated Venue Manager"))

		if self.social_login_key:
			doc = frappe.get_doc("Social Login Key", self.social_login_key)
		else:
			doc = frappe.new_doc("Social Login Key")
			doc.provider_name = self.identification

		doc.enable_social_login = 0
		doc.client_id = self.client_id
		doc.client_secret = self.get_password(fieldname="client_secret")
		doc.custom_base_url = 1
		doc.base_url = self.base_url
		doc.social_login_provider = "Custom"
		doc.authorize_url = doc.authorize_url or "/api/method/frappe.integrations.oauth2.authorize"
		doc.access_token_url = doc.access_token_url or "/api/method/frappe.integrations.oauth2.get_token"
		doc.redirect_url = f"/api/method/venues_federation.api.login_and_checkin/{self.identification}"
		doc.api_endpoint = doc.api_endpoint or "/api/method/frappe.integrations.oauth2.openid_profile"
		doc.auth_url_data = '{"response_type": "code", "scope": "openid"}'

		doc.save(ignore_permissions=True)
		self.social_login_key = doc.name

	def delete_social_login_key(self):
		if self.social_login_key:
			frappe.delete_doc("Social Login Key", self.social_login_key, force=True)
			self.social_login_key = None

	@frappe.whitelist()
	def create_webhook(self, secret):
		return frappe.get_doc({
			"doctype": "Webhook",
			"webhook_doctype": "Federated Venue Checkin",
			"webhook_docevent": "after_insert",
			"enabled": 1,
			"request_url": f"{self.base_url}/api/method/federated_venue.api.checkin_callback",
			"request_structure": "Form URL-Encoded",
			"enable_security": 1,
			"webhook_secret": secret,
			"condition": f"'{self.name}' in (doc.provider_venue, doc.client_venue)",
			"webhook_headers": [
				{
					"key": "Content-Type",
					"value": "application/x-www-form-urlencoded"
				}
			],
			"webhook_data": [
				{
					"fieldname": "name",
					"key": "checkin_id"
				},
				{
					"fieldname": "datetime",
					"key": "datetime"
				},
				{
					"fieldname": "customer",
					"key": "customer"
				},
				{
					"fieldname": "user",
					"key": "user"
				},
				{
					"fieldname": "title",
					"key": "title"
				},
				{
					"fieldname": "client_venue",
					"key": "client_venue"
				},
				{
					"fieldname": "client_venue_name",
					"key": "client_venue_name"
				},
				{
					"fieldname": "provider_venue",
					"key": "provider_venue"
				},
				{
					"fieldname": "provider_venue_name",
					"key": "provider_venue_name"
				}
			]
		}).insert()

	@property
	def about_cards(self):
		keys = ["title", "subtitle", "logo", "list_icon", "list", "footer"]
		defaults = {
			1: {
				"logo": "/images/fleur-coche.svg",
				"list_icon": "/images/icons/check.svg",
			},
			2: {
				"logo": "/images/plus-cercle.svg",
				"list_icon": "/images/icons/credit-card-plus.svg",
			},
			3: {
				"logo": "/images/papier-stylo.svg",
				"list_icon": "/images/icons/arrow-circle.svg",
			},
		}
		cards = []
		for i in range(1, 4):
			has_card = False
			card = frappe._dict()
			for key in keys:
				card[key] = self.get(f"about_card_{i}_{key}")
				if not card[key]:
					card[key] = defaults.get(i, {}).get(key)
				elif key in ("title", "subtitle", "list"):
					has_card = True
			if has_card:
				cards.append(card)
		return cards

	@frappe.whitelist()
	def get_qrcode(self):
		if url := self.connection_url:
			return {
				"url": url,
				"svg": get_qrcode_image(url),
			}


@frappe.whitelist(methods=["GET"])
def download_qrcode(name: str):
	venue: "FederatedVenue" = frappe.get_doc("Federated Venue", name)  # type: ignore
	venue.check_permission("read")
	png = get_qrcode_image(venue.connection_url, "png")
	frappe.local.response.filename = "qrcode.png"
	frappe.local.response.filecontent = png
	frappe.local.response.type = "download"


@frappe.whitelist(methods=["GET"])
def download_affiche(name: str):
	from venues_federation.affiche.generator import generate_affiche

	venue: "FederatedVenue" = frappe.get_doc("Federated Venue", name)  # type: ignore
	venue.check_permission("read")
	pdf = generate_affiche(venue).getvalue()
	frappe.local.response.filename = f"Affiche BIL - {venue.label}.pdf"
	frappe.local.response.filecontent = pdf
	frappe.local.response.type = "download"


def get_list_context(context):
	context.no_cache = True
	context.full_width = True
	context.show_sidebar = False
	context.title = frappe._("Federated Venues")


def does_auth_provider_redirect_to_checkin(social_login_key: str | None):
	if not social_login_key or not isinstance(social_login_key, str):
		return False

	social_login_redirect_url: str = frappe.get_value("Social Login Key", social_login_key, "redirect_url")  # type: ignore
	if "login_and_checkin" in social_login_redirect_url:
		return True

	return False

# def url_equals(left: str | None, right: str | None) -> bool:
# 	from urllib.parse import urlparse
# 	if not isinstance(left, str) or not isinstance(right, str):
# 		return False
# 	if not left or not right:
# 		return False
# 	a = urlparse(left)
# 	b = urlparse(right)
# 	return a.netloc == b.netloc and a.path.strip("/") == b.path.strip("/")
