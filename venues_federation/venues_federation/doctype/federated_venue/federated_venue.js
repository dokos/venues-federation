// Copyright (c) 2021, Dokos SAS and contributors
// For license information, please see license.txt

frappe.ui.form.on('Federated Venue', {
	setup(frm) {
		frm.set_query('user', 'managers', function() {
			return {
				query: "frappe.core.doctype.user.user.user_query",
				filters: {ignore_user_type: 1}
			};
		});
	},

	refresh(frm) {
		if (!frm.is_new()) {
			if (frm.doc.__onload && frm.doc.__onload.has_sso_login) {
				frm.dashboard.set_headline_alert(__("This venue uses SSO login provided by: {0}.", [frm.doc.social_login_key]), "green");
			} else if (frm.doc.base_url) {
				const url = `${frappe.urllib.get_base_url()}/api/method/venues_federation.api.login_and_checkin/${frm.doc.name}`;
				const url_html = `<b style="user-select:all;">${url}</b>`;
				const message = `<div style="user-select:none;">
					<div>${__("1. Create a new Oauth Client in website")} <a target="_blank" href="${frm.doc.base_url}">${frm.doc.base_url}</a></div>
					<div>${__("2. Add the following URLs in redirect URIs:")} ${url_html}</div>
					<div>${__("3. Add the following URL as the default redirect URI:")} ${url_html}</div>
					<div>${__("4. Copy and paste the generated client and secret key")}</div>
				</div>`;
				frm.dashboard.set_headline_alert(message);
			}
		}

		frm.trigger("setup_qrcode");
	},

	create_a_webhook(frm) {
		frappe.prompt({
			fieldtype: "Data",
			label: __("Webhook Secret"),
			fieldname: "webhook_secret",
			reqd: 1
		}, (data) => {
			frappe.call({
				method: "create_webhook",
				doc: frm.doc,
				args: {
					secret: data.webhook_secret
				}
			}).then(r => {
				if (r.message && r.message.name) {
					frm.set_value("webhook", r.message.name);
				}
			});
		});
	},

	async setup_qrcode(frm) {
		const { message: qrcode } = await frm.call("get_qrcode");
		if (qrcode) {
			const qrcode_download_url = "/api/method/venues_federation.venues_federation.doctype.federated_venue.federated_venue.download_qrcode?name=" + frm.doc.name;
			const pdf_download_url = "/api/method/venues_federation.venues_federation.doctype.federated_venue.federated_venue.download_affiche?name=" + frm.doc.name;
			frm.set_df_property("qrcode_html", "hidden", 0);
			frm.set_df_property("qrcode_html", "options", `
				<div class="d-flex align-center flex-column">
					${qrcode.svg}
					<a href="${qrcode_download_url}" class="underline">${"Télécharger le QR code seul"}</a>
					<br />
					<a href="${pdf_download_url}" class="btn btn-primary">${"Télécharger l'affiche en PDF"}</a>
					<br />
				</div>
			`);
		} else {
			frm.set_df_property("qrcode_html", "hidden", 1);
			frm.set_df_property("qrcode_html", "options", "");
		}
	},
});
