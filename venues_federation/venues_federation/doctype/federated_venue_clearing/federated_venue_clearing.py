# Copyright (c) 2022, Dokos SAS and contributors
# For license information, please see license.txt

import frappe
from frappe import _
from frappe.model.document import Document

class FederatedVenueClearing(Document):
	def validate(self):
		print([r for r in self.references if r.client_venue == self.paying_venue])
		print([r for r in self.references if r.client_venue == self.receiving_venue])
		self.paid_quantity = (
			len([r for r in self.references if r.client_venue == self.paying_venue]) -
			len([r for r in self.references if r.client_venue == self.receiving_venue])
		)

		if self.paid_quantity < 0:
			frappe.throw(_("You cannot make a clearing with a negative net total"))

	def on_submit(self):
		self.update_references_status("Cleared")

	def on_cancel(self):
		self.update_references_status("Pending")

	def update_references_status(self, status):
		for doc in self.references:
			frappe.db.set_value("Federated Venue Checkin Summary", doc.reference_name, "status", status)


@frappe.whitelist()
def get_references(paying_venue, receiving_venue, unit_of_measure):
	return frappe.get_all("Federated Venue Checkin Summary",
		filters={
			"status": "Pending",
			"accounted_duration": unit_of_measure,
			"client_venue": ("in", (paying_venue, receiving_venue)),
			"provider_venue": ("in", (paying_venue, receiving_venue)),
		},
		fields=[
			"name as reference_name",
			"accounted_duration as unit_of_measure",
			"customer",
			"client_venue",
			"provider_venue"
		]
	)
