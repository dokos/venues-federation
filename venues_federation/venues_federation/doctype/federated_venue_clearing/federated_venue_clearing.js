// Copyright (c) 2022, Dokos SAS and contributors
// For license information, please see license.txt

frappe.ui.form.on('Federated Venue Clearing', {
	refresh(frm) {
		frm.trigger("add_references_button")
	},

	add_references_button(frm) {
		frm.add_custom_button(__('Get references'), function() {
			if (!frm.doc.paying_venue || !frm.doc.receiving_venue || !frm.doc.unit_of_measure) {
				frappe.msgprint(__("Please fill in all mandatory fields first."))
			} else {
				frappe.call({
					method: "venues_federation.venues_federation.doctype.federated_venue_clearing.federated_venue_clearing.get_references",
					args: {
						paying_venue: frm.doc.paying_venue,
						receiving_venue: frm.doc.receiving_venue,
						unit_of_measure: frm.doc.unit_of_measure
					}
				}).then(r => {
					frm.clear_table('references')
					r.message.forEach((d) => {
						frm.add_child("references", d);
					});
					frm.refresh_field("references");

					frm.trigger("calculate_total")
				})
			}
		});
	},

	calculate_total(frm) {
		const paid_quantity = (
			frm.doc.references.filter(f => f.client_venue == frm.doc.paying_venue).length - 
			frm.doc.references.filter(f => f.client_venue == frm.doc.receiving_venue).length
		)
		frm.set_value("paid_quantity", paid_quantity)
	}
});
