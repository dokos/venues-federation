# Copyright (c) 2021, Dokos SAS and contributors
# For license information, please see license.txt

from itertools import zip_longest

import frappe
from frappe import _
from frappe.utils import time_diff_in_seconds, get_datetime, flt
from frappe.website.website_generator import WebsiteGenerator

class FederatedVenueCheckin(WebsiteGenerator):
	def validate(self):
		super(FederatedVenueCheckin, self).validate()
		self.title = f"{self.customer} - {self.user}"

	def set_route(self):
		self.route = f"checkins/{self.name}"
		super(FederatedVenueCheckin, self).set_route()

	def on_update(self):
		super(FederatedVenueCheckin, self).on_update()
		self.calculate_summary()

	def after_delete(self):
		self.calculate_summary()

	def calculate_summary(self):
		if self.venue_checkin_summary:
			dates = frappe.get_all("Federated Venue Checkin",
				filters={"venue_checkin_summary": self.venue_checkin_summary},
				order_by="datetime asc",
				pluck="datetime"
			)
			if dates:
				day_end = dates[0].replace(hour=23, minute=59, second=59)
				dates = [iter([get_datetime(d) for d in dates])] * 2
				date_groups = list(zip_longest(*dates, fillvalue=day_end))
				total = sum(time_diff_in_seconds(group[1], group[0]) for group in date_groups)
			else:
				total = 0.0

			accounted_duration = (
				"Half-Day" if 0.0 < total <= flt(
					frappe.db.get_single_value("Venues Federation Settings", "half_day_duration")
				) else (None if total == 0.0 else "Full-Day")
			)
			frappe.db.set_value("Federated Venue Checkin Summary", self.venue_checkin_summary, "full_duration", total)
			frappe.db.set_value("Federated Venue Checkin Summary", self.venue_checkin_summary, "accounted_duration", accounted_duration)

	def get_context(self, context):
		if frappe.session.user=='Guest':
			frappe.throw(_("You need to be logged in to access this page"), frappe.PermissionError)

		context.show_sidebar = True

def get_list_context(context):
	context.title = _("Checkins History")
	context.show_sidebar = True
	context.header_action = frappe.render_template("venues_federation/doctype/federated_venue_checkin/templates/federated_venue_checkin_list_action.html", {})
	context.get_list = get_checkins_list

def get_checkins_list(doctype, txt, filters, limit_start, limit_page_length = 20, order_by = None):
	from frappe.www.list import get_list
	from itertools import groupby

	query = get_list(
		doctype,
		txt,
		dict(user=frappe.session.user),
		limit_start,
		limit_page_length,
		ignore_permissions=True,
		order_by="venue_checkin_summary, datetime  desc"
	)

	grouped_query = [list(y) for x,y in groupby(query, lambda x: x['venue_checkin_summary'])]
	result = []
	for group in grouped_query:
		line = frappe._dict()
		for row in sorted(group, key=lambda x: x['datetime']):
			if line:
				line["datetime"].append(row.datetime)
			else:
				line = row
				line["datetime"] = [row.datetime]

			line["status"] = "Completed" if row.venue_checkin_summary else "Pending"
			if line["status"] == "Completed":
				line["duration"], line["accounted_duration"] = frappe.db.get_value("Federated Venue Checkin Summary", row.venue_checkin_summary, ["full_duration", "accounted_duration"])

		result.append(line)

	return result