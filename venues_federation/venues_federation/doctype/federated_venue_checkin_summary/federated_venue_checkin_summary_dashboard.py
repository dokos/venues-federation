# Copyright (c) 2022, Dokos SAS and contributors
# For license information, please see license.txt
from frappe import _


def get_data():
	return {
		'fieldname': 'reference_name',
		'transactions': [
			{
				'label': _('Federated Venue Clearing'),
				'items': ['Federated Venue Clearing']
			}
		]
	}