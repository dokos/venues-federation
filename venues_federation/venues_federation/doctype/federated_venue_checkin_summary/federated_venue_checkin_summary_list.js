// Copyright (c) 2022, Dokos SAS and contributors
// For license information, please see license.txt

frappe.listview_settings['Federated Venue Checkin Summary'] = {
	get_indicator: function(doc) {
		if (doc.status === "Pending") {
			return [__("Pending"), "orange", "status,=,Pending"];
		} else if (doc.status === "Cleared") {
			return [__("Cleared", null, "Federated Venue Checkin Summary"), "green", "status,=,Cleared"];
		}
	}
}
