# Copyright (c) 2022, Dokos SAS and contributors
# For license information, please see license.txt
from typing import Union

import frappe
from frappe.utils import format_date, getdate
from frappe.model.document import Document

from venues_federation.venues_federation.doctype.federated_venue_checkin.federated_venue_checkin import FederatedVenueCheckin

class FederatedVenueCheckinSummary(Document):
	def validate(self):
		self.title = f"{self.customer} - {self.user} - {format_date(self.date)}"


def find_and_associate_venue_checkin(checkin: FederatedVenueCheckin) -> None:
	summary = get_existing_checkin_summary(checkin)
	if not summary:
		summary = frappe.get_doc(dict(
			doctype = "Federated Venue Checkin Summary",
			date = getdate(checkin.datetime),
			client_venue = checkin.client_venue,
			provider_venue = checkin.provider_venue,
			customer = checkin.customer,
			user = checkin.user
		)).insert(ignore_mandatory=True)

	checkin.venue_checkin_summary = summary.name
	checkin.save()


def get_existing_checkin_summary(checkin: FederatedVenueCheckin) -> Union[FederatedVenueCheckinSummary, None]:
	"""
	Returns an existing checkin summary for a particular day, client venue, customer and user.
	If there is
	"""
	try:
		return frappe.get_doc("Federated Venue Checkin Summary", dict(
			date = getdate(checkin.datetime),
			client_venue = checkin.client_venue,
			provider_venue = checkin.provider_venue,
			customer = checkin.customer,
			user = checkin.user
		))
	except frappe.exceptions.DoesNotExistError:
		pass