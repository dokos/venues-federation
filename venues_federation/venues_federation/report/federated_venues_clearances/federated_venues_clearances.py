# Copyright (c) 2022, Dokos SAS and contributors
# For license information, please see license.txt

from collections import defaultdict

import frappe
from frappe.utils import flt
from frappe import _

from venues_federation.venues_federation.report.federated_venues_consumption.federated_venues_consumption import get_summary_by_provider

def execute(filters=None):
	columns, data = get_columns(filters), get_data(filters)
	return columns, data

def get_columns(filters=None):
	return [
		{
			"fieldname": "paying_venue",
			"fieldtype": "Link",
			"label": _("Paying Venue"),
			"options": "Federated Venue",
			"width": 400
		},
		{
			"fieldname": "receiving_venue",
			"fieldtype": "Link",
			"label": _("Receiving Venue"),
			"options": "Federated Venue",
			"width": 400
		},
		{
			"fieldname": "qty",
			"fieldtype": "Float",
			"label": _("Quantity"),
			"width": 100
		},
		{
			"fieldname": "unit_of_measure",
			"fieldtype": "Data",
			"label": _("Unit of Measure"),
			"width": 150
		}
	]

def get_data(filters=None):
	federated_venues_labels = {x.name: x.label for x in frappe.get_all("Federated Venue", fields=["name", "label"])}

	query_filters={"status": "Pending"}
	list_by_provider = get_summary_by_provider(query_filters)

	selected_venues = [x for x in list_by_provider]

	balance = defaultdict(lambda: defaultdict(lambda: defaultdict(float)))
	for provider in selected_venues:
		for client in list_by_provider[provider]:
			for uom in list_by_provider[provider][client]:
				balance[provider][client][uom] = flt(list_by_provider[provider][client][uom]) - flt(list_by_provider.get(client, {}).get(provider, {}).get(uom))

	result = []
	for paying_venue in balance:
		for receiving_venue in balance[paying_venue]:
			for uom in balance[paying_venue][receiving_venue]:
				if balance[paying_venue][receiving_venue][uom] > 0.0:
					result.append({
						"paying_venue": federated_venues_labels.get(paying_venue, paying_venue),
						"receiving_venue": federated_venues_labels.get(receiving_venue, receiving_venue),
						"qty": abs(balance[paying_venue][receiving_venue][uom]),
						"unit_of_measure": _(uom)
					})

	return result