# Copyright (c) 2022, Dokos SAS and contributors
# For license information, please see license.txt

from collections import defaultdict

import frappe
from frappe import _

def execute(filters=None):
	columns, data = get_columns(filters), get_data(filters)
	return columns, data

def get_columns(filters=None):
	return [
		{
			"fieldname": "provider_venue",
			"fieldtype": "Link",
			"label": _("Provider Venue"),
			"options": "Federated Venue",
			"width": 400
		},
		{
			"fieldname": "client_venue",
			"fieldtype": "Link",
			"label": _("Client Venue"),
			"options": "Federated Venue",
			"width": 400
		},
		{
			"fieldname": "qty",
			"fieldtype": "Float",
			"label": _("Quantity"),
			"width": 100
		},
		{
			"fieldname": "unit_of_measure",
			"fieldtype": "Data",
			"label": _("Unit of Measure"),
			"width": 150
		}
	]

def get_data(filters=None):
	federated_venues_labels = {x.name: x.label for x in frappe.get_all("Federated Venue", fields=["name", "label"])}

	query_filters = {}
	if filters.from_date and filters.to_date:
		query_filters.update({
			"date": ("between", (filters.from_date, filters.to_date))
		})

	list_by_provider = get_summary_by_provider(query_filters)

	output = []
	for provider in list_by_provider:
		row = {
			"provider_venue": federated_venues_labels.get(provider, provider) or _("Undefined")
		}
		for client in list_by_provider[provider]:
			row.update({
				"client_venue": federated_venues_labels.get(client, client)
			})
			for uom in list_by_provider[provider][client]:
				row.update({
					"unit_of_measure": _(uom),
					"qty": list_by_provider[provider][client][uom]
				})
				output.append(row)
				row = {"provider_venue": None, "client_venue": None}

	return output

def get_summary_by_provider(query_filters=None):
	summary = frappe.get_all("Federated Venue Checkin Summary", 
		filters=(query_filters or {}),
		fields=["date", "accounted_duration", "client_venue", "provider_venue"]
	)

	list_by_provider = defaultdict(lambda: defaultdict(lambda: defaultdict(float)))
	for summ in summary:
		if summ.provider_venue != summ.client_venue and summ.accounted_duration:
			list_by_provider[summ.provider_venue][summ.client_venue][summ.accounted_duration] += 1.0

	return list_by_provider