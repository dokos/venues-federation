frappe.ready(() => {
	const searchInput = document.getElementById("social-login-search");
	searchInput.addEventListener("input", searchFuntion);

	function searchFuntion(e) {
		const filter = e.target.value.toUpperCase();

		const list = document.getElementById("social-login-cards");
		const cards = list.getElementsByClassName("social-card");
		for (var i = 0; i < cards.length; i++) {
			const label = cards[i].getAttribute("data-label");
			let filters = filter.split(" ");
			filters = filters.filter(f => f.length);

			let shouldDisplay = true;
			filters.forEach(filt => {
				shouldDisplay = shouldDisplay && label.toUpperCase().includes(filt);
			});

			cards[i].style.display = (shouldDisplay || filters.length === 0) ? "" : "none";
		}
	}
});
