import frappe

def execute():
	settings = frappe.get_single("Website Settings")
	settings.app_name = "Badge Inter-Lieux"
	settings.disable_signup = 1
	settings.save()