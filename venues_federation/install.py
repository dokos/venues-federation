import frappe


def after_install():
	# initialize_site()
	configure_portal()
	make_roles()
	frappe.db.set_single_value("Website Settings", "website_theme", "Federation")
	frappe.db.set_single_value("Website Settings", "app_name", "Badge Inter-Lieux")


def after_migrate():
	make_roles()

def make_roles():
	def make_role(role_name, desk_access):
		if frappe.db.exists("Role", role_name):
			role = frappe.get_doc("Role", role_name)
		else:
			role = frappe.new_doc("Role")
			role.role_name = role_name
		role.desk_access = desk_access
		role.save()

	make_role("Federated Venue Admin", 1)
	make_role("Federated Venue Manager", 1)
	make_role("Federated Venue User", 0)


def initialize_site():
	from frappe.desk.page.setup_wizard.setup_wizard import setup_complete

	completed = setup_complete(
		{
			"full_name": "Charles-Henri Decultot",
			"email": "hello@dokos.io",
			"password": "bnXuHt3qVHc8",
			"currency": "EUR",
			"timezone": "Europe/Paris",
			"country": "France",
			"language": "Français",
			"lang": "fr",
			"demo": 1,
		}
	)
	print("Initialization completed", completed)


def configure_portal():
	portal_settings = frappe.get_single("Portal Settings")
	portal_settings.hide_standard_menu = 1
	for menu in [
		{
			"title": "Enregistrements",
			"enabled": 1,
			"route": "/checkins",
			"reference_doctype": "Federated Venue Checkin",
		},
		{
			"title": "Utilisateurs",
			"enabled": 1,
			"route": "/whitelisted-users",
			"reference_doctype": "Federated Venue Whitelist",
			"role": "Federated Venue Manager",
		},
	]:
		portal_settings.append("custom_menu", menu)
	portal_settings.save()
