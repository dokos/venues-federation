from frappe import _

def get_data():
	return [
		{
			"module_name": "Venues Federation",
			"color": "grey",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": _("Venues Federation")
		}
	]
