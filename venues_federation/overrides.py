import frappe
from frappe.website.doctype.web_form.web_form import get_form_data


@frappe.whitelist()
def get_whitelist_form_data(doctype, docname=None, web_form_name=None):
	out = get_form_data(doctype, docname, web_form_name)

	for field in out.web_form.web_form_fields:
		if field.fieldname == "venue":
			field.options = [
				{"value": doc.name, "description": doc.label}
				for doc in frappe.get_list("Federated Venue", fields=["name", "label"])
			]
	# print("out", [x.as_dict() for x in out.web_form.web_form_fields])
	return out
