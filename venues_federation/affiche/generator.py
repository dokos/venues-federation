from io import BytesIO
from typing import TYPE_CHECKING, Literal
import frappe
from frappe.utils import get_url
import pymupdf
import colorsys

if TYPE_CHECKING:
	from venues_federation.venues_federation.doctype.federated_venue.federated_venue import FederatedVenue


class AnnotMatcher:
	COLOR_TO_HUE_MAP = [
		("red", 0),
		("green", 120),
		("blue", 240),
	]
	ANNOT_TYPES = [
		("rectangle", pymupdf.mupdf.PDF_ANNOT_SQUARE),
	]

	def matches(self, a: pymupdf.Annot, color: Literal["red", "green", "blue"], type: Literal["rectangle"]):
		if self.get_type(a) == type:
			if self.get_color(a) == color:
				return True
		return False

	def get_type(self, a: pymupdf.Annot):
		for annot_type_name, annot_type_id in self.ANNOT_TYPES:
			if a.type[0] == annot_type_id:
				return annot_type_name

	def get_color(self, a: pymupdf.Annot):
		for color_name, color_hue in self.COLOR_TO_HUE_MAP:
			stroke_hue = self._get_hue(a.colors.get("stroke"))
			if stroke_hue is not None:
				if self._hue_dist(stroke_hue, color_hue) < 10:
					return color_name

			fill_hue = self._get_hue(a.colors.get("fill"))
			if fill_hue is not None:
				if self._hue_dist(fill_hue, color_hue) < 10:
					return color_name

	def _get_hue(self, rgb: tuple[float, float, float] | None) -> float | None:
		if not rgb or len(rgb) < 3 or sum(rgb) <= 0:
			return None
		return colorsys.rgb_to_hsv(*rgb)[0] * 360

	def _hue_dist(self, a: float | None, b: float | None) -> float:
		if a is None or b is None:
			return float("inf")
		if a < 0 or b < 0:
			return float("inf")
		diff = abs(a - b)
		return min(diff, 360 - diff)


def generate_affiche(venue: "FederatedVenue | str"):
	from venues_federation.venues_federation.doctype.federated_venue.federated_venue import get_qrcode_image

	if isinstance(venue, str):
		_loaded_venue: "FederatedVenue" = frappe.get_doc("Federated Venue", venue)  # type: ignore
		venue = _loaded_venue

	TEMPLATE_PATH = frappe.get_app_path("venues_federation", "affiche", "template.pdf")
	FONTS_PATH = frappe.get_app_path("venues_federation", "affiche", "fonts")
	FONT_FILE = "./DIN Condensed Bold.ttf"

	with pymupdf.open(TEMPLATE_PATH) as doc:
		page = doc[0]
		annot_matcher = AnnotMatcher()
		annotations_to_remove: list[pymupdf.Annot] = []

		for annot in page.annots():
			if annot_matcher.matches(annot, "red", "rectangle"):
				annotations_to_remove.append(annot)
				size = pymupdf.Point(9999, annot.rect.height)
				center = (annot.rect.top_left + annot.rect.bottom_right) / 2
				bbox = pymupdf.Rect(p1=center + size/2, p0=center - size/2)
				fonts = pymupdf.Archive(FONTS_PATH)
				css = (
					f"""
					* {{
						font-family: titleFont;
						color: #193145;
						font-size: {bbox.height}px;
						text-align: center;
						line-height: {bbox.height}px;
						font-weight: bolder;
					}}
					@font-face {{
						font-family: titleFont;
						src: url({FONT_FILE!r});
						font-weight: bolder;
					}}
					"""
				)
				pymupdf.utils.insert_htmlbox(
					page,
					bbox,
					venue.label.upper(),
					css=css,
					archive=fonts,
				)

			if annot_matcher.matches(annot, "blue", "rectangle"):
				annotations_to_remove.append(annot)
				size = max(annot.rect.width, annot.rect.height)
				size_vec = pymupdf.Point(size, size)
				center = (annot.rect.top_left + annot.rect.bottom_right) / 2
				bbox = pymupdf.Rect(
					p1=center + size_vec/2,
					p0=center - size_vec/2,
				)
				url = get_url(f"/L/{venue.name}").upper()
				qrcode = get_qrcode_image(url, "png", scale=16, fg="#193145")
				pymupdf.utils.insert_image(page, bbox, stream=qrcode)

		for annot in annotations_to_remove:
			page.delete_annot(annot)

		out = BytesIO()
		doc.save(out)

		return out
